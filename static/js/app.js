"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    class: '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts.class = opts.class ? " ".concat(opts.class) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts.class, "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  $('.js-answers-questions-acc').on('click', function () {
    var $this = $(this),
        $parent = $this.parent(),
        $content = $this.next();
    $this.toggleClass('js-answers-questions-is-active');
    $parent.toggleClass('js-answers-questions-is-active');
    $content.slideToggle(300);
  });
  $('.js-certificates').owlCarousel({
    items: 5,
    loop: true,
    autoWidth: true,
    margin: 30,
    nav: true,
    dots: false,
    responsive: {
      0: {
        items: 1,
        autoWidth: false,
        autoHeight: true
      },
      481: {
        items: 5,
        autoWidth: true,
        autoHeight: false
      }
    }
  });
  $('.lb-outerContainer').append('<div class="lb-data"><a class="lb-close"></a></div>');
  $('.lb-outerContainer .lb-close').bind('click', function () {
    $('.lb-dataContainer .lb-close').trigger('click');
  });
  $('.popup-form .b-form__form').on('submit', function (event) {
    var $this = $(this);
    event.preventDefault();
    $this.closest('.b-form__form').addClass('success');
    $this.closest('.popup-form').find('.b-form__send-success').addClass('success');
    setTimeout(function () {
      $this.closest('.app').find('.remodal-overlay').css('display', 'none');
      $this.closest('.app').find('.remodal-wrapper').css('display', 'none');
      $('html').removeClass('remodal-is-locked');
      $('body').css('padding-right', 0);
    }, 3000);
    setTimeout(function () {
      $this.closest('.b-form__form').removeClass('success');
      $this.closest('.popup-form').find('.b-form__send-success').removeClass('success');
    }, 3000);
  });
  $('.b-form.b-form--girl').on('submit', function (event) {
    event.preventDefault();
    $('[data-remodal-id=formGirl]').remodal().open();
    $('.b-form__input input').val('');
    setTimeout(function () {
      $('body').closest('.app').find('.remodal-overlay').css('display', 'none');
      $('body').closest('.app').find('.remodal-wrapper').css('display', 'none');
      $('html').removeClass('remodal-is-locked');
      $('body').css('padding-right', 0);
    }, 3000);
  });
  $('.b-form.b-form--bottom').on('submit', function (event) {
    event.preventDefault();
    $('[data-remodal-id=formBot]').remodal().open();
    $('.b-form__input input').val('');
    setTimeout(function () {
      $('body').closest('.app').find('.remodal-overlay').css('display', 'none');
      $('body').closest('.app').find('.remodal-wrapper').css('display', 'none');
      $('html').removeClass('remodal-is-locked');
      $('body').css('padding-right', 0);
    }, 3000);
  });
  $('.js-form-timer').countdown('2020/12/30', function (event) {
    var $this = $(this).html(event.strftime('' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%d</div> <div class="b-form__timer-date">дн.</div> </div><span>:</span>' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%H</div> <div class="b-form__timer-date">час.</div> </div><span>:</span>' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%M</div> <div class="b-form__timer-date">мин.</div> </div><span>:</span>' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%S</div> <div class="b-form__timer-date">сек.</div></div>'));
  });
  $('.js-form-timer-girl').countdown('2020/12/30', function (event) {
    var $this = $(this).html(event.strftime('' // + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%d</div> <div class="b-form__timer-date">дн.</div> </div><span>:</span>'
    // + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%H</div> <div class="b-form__timer-date">час.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%M</div> <div class="b-form__timer-date">мин.</div> </div><span>:</span>' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%S</div> <div class="b-form__timer-date">сек.</div></div>'));
  });
  $('.js-form-specialist-timer').countdown('2020/12/30', function (event) {
    var $this = $(this).html(event.strftime('' // + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%d</div> <div class="b-form__timer-date">дн.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%H</div> <div class="b-form__timer-date">час.</div> </div><span>:</span>' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%M</div> <div class="b-form__timer-date">мин.</div> </div><span>:</span>' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%S</div> <div class="b-form__timer-date">сек.</div></div>'));
  });
  $('.js-form-share-timer').countdown('2020/12/30', function (event) {
    var $this = $(this).html(event.strftime('' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%d</div> <div class="b-form__timer-date">дн.</div> </div><span>:</span>' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%H</div> <div class="b-form__timer-date">час.</div> </div><span>:</span>' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%M</div> <div class="b-form__timer-date">мин.</div> </div><span>:</span>' + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%S</div> <div class="b-form__timer-date">сек.</div></div>'));
  });
  $('.js-btn-burger').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('btn-burger-is-active');
    $('.b-mobile-panel .b-menu').toggleClass('menu-is-active');
    $('.b-mobile-panel__overlay-menu').toggleClass('overlay-is-active');
  });
  $('.js-our-specialists-slider').owlCarousel({
    items: 1,
    loop: true,
    margin: 70,
    nav: true,
    dots: true,
    smartSpeed: 500,
    mouseDrag: false,
    navText: ['', '']
  });
  $(".js-before-after-init").twentytwenty({
    orientation: 'horizontal',
    no_overlay: true,
    click_to_move: true
  });
  setTimeout(function () {
    $('.js-reviews-slider').addClass('owl-carousel').owlCarousel({
      items: 1,
      loop: true,
      margin: 0,
      nav: true,
      dots: true,
      smartSpeed: 500,
      mouseDrag: false,
      touchDrag: false,
      navText: ['', '']
    });
  }, 1000);
  $('.js-share-timer').countdown('2020/12/30', function (event) {
    var $this = $(this).html(event.strftime('' + '<div class="b-share__timer-item"> <div class="b-share__timer-namber">%d</div> <div class="b-share__timer-date">дн.</div> </div><span>:</span>' + '<div class="b-share__timer-item"> <div class="b-share__timer-namber">%H</div> <div class="b-share__timer-date">час.</div> </div><span>:</span>' + '<div class="b-share__timer-item"> <div class="b-share__timer-namber">%M</div> <div class="b-share__timer-date">мин.</div> </div><span>:</span>' + '<div class="b-share__timer-item"> <div class="b-share__timer-namber">%S</div> <div class="b-share__timer-date">сек.</div></div>'));
  });
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  $('.js-nav-to-anchor a').on('click', function (e) {
    e.preventDefault();
    var anchor = $(this);
    $('.js-btn-burger').removeClass('btn-burger-is-active');
    $('.b-mobile-panel__overlay-menu').removeClass('overlay-is-active');
    $('.b-mobile-panel__nav .b-menu').removeClass('menu-is-active');
    $('html, body').stop().animate({
      scrollTop: $(anchor.attr('href')).offset().top
    }, 800);
    return false;
  });
  $('.b-about-prices__item').on('click', '.js-about-prices-more', function () {
    $(this).find('.js-about-prices-text').toggleClass('more-text-is-active');
  });
  $(document).on('click', function (e) {
    if ($(e.target).closest('.b-about-prices__more').length) {
      return;
    }

    $('.js-about-prices-text').removeClass('more-text-is-active');
  });
  $('.js-first-screen-inner-timer').countdown('2020/12/30', function (event) {
    var $this = $(this).html(event.strftime('' // + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%d</div> <div class="b-form__timer-date">дн.</div> </div><span>:</span>'
    + '<div class="b-first-screen-info__timer-item"> <div class="b-first-screen-info__timer-namber">%H</div> <div class="b-first-screen-info__timer-date">час.</div> </div><span>:</span>' + '<div class="b-first-screen-info__timer-item"> <div class="b-first-screen-info__timer-namber">%M</div> <div class="b-first-screen-info__timer-date">мин.</div> </div><span>:</span>' + '<div class="b-first-screen-info__timer-item"> <div class="b-first-screen-info__timer-namber">%S</div> <div class="b-first-screen-info__timer-date">сек.</div></div>'));
  });
  $('.js-reviews-text-custom-scroll').each(function () {
    $(this).scrollbox();
  });
});