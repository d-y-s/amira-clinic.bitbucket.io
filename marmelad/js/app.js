/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = Object.assign({
    tag: 'div',
    type: 'icons',
    class: '',
    mode: 'inline',
    url: '',
  }, opts);

  let external = '';
  let typeClass = '';

  if (opts.mode === 'external') {
    external = `${opts.url}/sprite.${opts.type}.svg`;
  }

  if (opts.type !== 'icons') {
    typeClass = ` svg-icon--${opts.type}`;
  }

  opts.class = opts.class ? ` ${opts.class}` : '';

  return `
    <${opts.tag} class="svg-icon svg-icon--${name}${typeClass}${opts.class}" aria-hidden="true" focusable="false">
      <svg class="svg-icon__link">
        <use xlink:href="${external}#${name}"></use>
      </svg>
    </${opts.tag}>
  `;
}

/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function() {

  'use strict';

  /**
   * определение существования элемента на странице
   */
  $.exists = (selector) => $(selector).length > 0;

  //=require ../_blocks/**/*.js

  $('.js-nav-to-anchor a').on('click', function (e) {
    e.preventDefault();
    var anchor = $(this);
    $('.js-btn-burger').removeClass('btn-burger-is-active');
    $('.b-mobile-panel__overlay-menu').removeClass('overlay-is-active');
    $('.b-mobile-panel__nav .b-menu').removeClass('menu-is-active');
    $('html, body').stop().animate({
      scrollTop: $(anchor.attr('href')).offset().top
    }, 800);
    return false;
  });

  $('.b-about-prices__item').on('click', '.js-about-prices-more', function() {
    $(this).find('.js-about-prices-text').toggleClass('more-text-is-active');
  });

  $(document).on('click', function (e) {
    if ($(e.target).closest('.b-about-prices__more').length) {
      return;
    }

    $('.js-about-prices-text').removeClass('more-text-is-active');
  });


  $('.js-first-screen-inner-timer').countdown('2020/12/30', function(event) {
  var $this = $(this).html(event.strftime(''
    // + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%d</div> <div class="b-form__timer-date">дн.</div> </div><span>:</span>'
    + '<div class="b-first-screen-info__timer-item"> <div class="b-first-screen-info__timer-namber">%H</div> <div class="b-first-screen-info__timer-date">час.</div> </div><span>:</span>'
    + '<div class="b-first-screen-info__timer-item"> <div class="b-first-screen-info__timer-namber">%M</div> <div class="b-first-screen-info__timer-date">мин.</div> </div><span>:</span>'
    + '<div class="b-first-screen-info__timer-item"> <div class="b-first-screen-info__timer-namber">%S</div> <div class="b-first-screen-info__timer-date">сек.</div></div>'));
  });

  $('.js-reviews-text-custom-scroll').each(function () {
    $(this).scrollbox();
  })

  

});

