$('.js-btn-burger').on('click', function(e) {
    e.preventDefault();
    $(this).toggleClass('btn-burger-is-active');

    $('.b-mobile-panel .b-menu').toggleClass('menu-is-active');
    $('.b-mobile-panel__overlay-menu').toggleClass('overlay-is-active')
});