$(".js-before-after-init").twentytwenty({
    orientation: 'horizontal',
    no_overlay: true, 
    click_to_move: true
});

setTimeout(function () {
    $('.js-reviews-slider').addClass('owl-carousel').owlCarousel({
    items: 1,
    loop: true,
    margin: 0,
    nav: true,
    dots: true,
    smartSpeed: 500,
    mouseDrag: false,
    touchDrag: false,
    navText: ['','']
    });
}, 1000)