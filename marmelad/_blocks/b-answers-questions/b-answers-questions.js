$('.js-answers-questions-acc').on('click', function () {
    var $this = $(this),
        $parent = $this.parent(),
        $content = $this.next();
    $this.toggleClass('js-answers-questions-is-active');
    $parent.toggleClass('js-answers-questions-is-active');
    $content.slideToggle(300);
  });