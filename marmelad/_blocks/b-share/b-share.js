$('.js-share-timer').countdown('2020/12/30', function(event) {
  var $this = $(this).html(event.strftime(''
    + '<div class="b-share__timer-item"> <div class="b-share__timer-namber">%d</div> <div class="b-share__timer-date">дн.</div> </div><span>:</span>'
    + '<div class="b-share__timer-item"> <div class="b-share__timer-namber">%H</div> <div class="b-share__timer-date">час.</div> </div><span>:</span>'
    + '<div class="b-share__timer-item"> <div class="b-share__timer-namber">%M</div> <div class="b-share__timer-date">мин.</div> </div><span>:</span>'
    + '<div class="b-share__timer-item"> <div class="b-share__timer-namber">%S</div> <div class="b-share__timer-date">сек.</div></div>'));
});