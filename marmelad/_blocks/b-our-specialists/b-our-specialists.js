$('.js-our-specialists-slider').owlCarousel({
    items: 1,
    loop: true,
    margin: 70,
    nav: true,
    dots: true,
    smartSpeed: 500,
    mouseDrag: false,
    navText: ['','']
});