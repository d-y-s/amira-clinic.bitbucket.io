$('.js-certificates').owlCarousel({
    items: 5,
    loop: true,
    autoWidth:true,
    margin: 30,
    nav: true,
    dots: false,
    responsive:{
        0:{
            items:1,
            autoWidth:false,
            autoHeight: true
        },
        481:{
            items:5,
            autoWidth:true,
            autoHeight: false
        }
    }
});

$('.lb-outerContainer').append('<div class="lb-data"><a class="lb-close"></a></div>');

$('.lb-outerContainer .lb-close').bind('click', function() {
    $('.lb-dataContainer .lb-close').trigger('click');
});