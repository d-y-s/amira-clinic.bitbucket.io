$('.popup-form .b-form__form').on('submit', function (event) { 
    var $this = $(this);
    event.preventDefault();
    $this.closest('.b-form__form').addClass('success');
    $this.closest('.popup-form').find('.b-form__send-success').addClass('success');
    setTimeout(function () {
        $this.closest('.app').find('.remodal-overlay').css('display', 'none');
        $this.closest('.app').find('.remodal-wrapper').css('display', 'none');
        $('html').removeClass('remodal-is-locked');
        $('body').css('padding-right', 0)
    }, 3000);
    setTimeout(function () {
        $this.closest('.b-form__form').removeClass('success');
        $this.closest('.popup-form').find('.b-form__send-success').removeClass('success');
    }, 3000);
});

$('.b-form.b-form--girl').on('submit', function (event) { 
    event.preventDefault();
    $('[data-remodal-id=formGirl]').remodal().open();
    $('.b-form__input input').val('');
    setTimeout(function () {
        $('body').closest('.app').find('.remodal-overlay').css('display', 'none');
        $('body').closest('.app').find('.remodal-wrapper').css('display', 'none');
        $('html').removeClass('remodal-is-locked');
        $('body').css('padding-right', 0)
    }, 3000);
});

$('.b-form.b-form--bottom').on('submit', function (event) { 
    event.preventDefault();
    $('[data-remodal-id=formBot]').remodal().open();
    $('.b-form__input input').val('');
    setTimeout(function () {
        $('body').closest('.app').find('.remodal-overlay').css('display', 'none');
        $('body').closest('.app').find('.remodal-wrapper').css('display', 'none');
        $('html').removeClass('remodal-is-locked');
        $('body').css('padding-right', 0)
    }, 3000);
});

$('.js-form-timer').countdown('2020/12/30', function(event) {
  var $this = $(this).html(event.strftime(''
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%d</div> <div class="b-form__timer-date">дн.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%H</div> <div class="b-form__timer-date">час.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%M</div> <div class="b-form__timer-date">мин.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%S</div> <div class="b-form__timer-date">сек.</div></div>'));
});

$('.js-form-timer-girl').countdown('2020/12/30', function(event) {
  var $this = $(this).html(event.strftime(''
    // + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%d</div> <div class="b-form__timer-date">дн.</div> </div><span>:</span>'
    // + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%H</div> <div class="b-form__timer-date">час.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%M</div> <div class="b-form__timer-date">мин.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%S</div> <div class="b-form__timer-date">сек.</div></div>'));
});

$('.js-form-specialist-timer').countdown('2020/12/30', function(event) {
  var $this = $(this).html(event.strftime(''
    // + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%d</div> <div class="b-form__timer-date">дн.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%H</div> <div class="b-form__timer-date">час.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%M</div> <div class="b-form__timer-date">мин.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%S</div> <div class="b-form__timer-date">сек.</div></div>'));
});

$('.js-form-share-timer').countdown('2020/12/30', function(event) {
  var $this = $(this).html(event.strftime(''
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%d</div> <div class="b-form__timer-date">дн.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%H</div> <div class="b-form__timer-date">час.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%M</div> <div class="b-form__timer-date">мин.</div> </div><span>:</span>'
    + '<div class="b-form__timer-item"> <div class="b-form__timer-namber">%S</div> <div class="b-form__timer-date">сек.</div></div>'));
});
